{ config, pkgs, ... }:

{
  imports = [
    ./amd-gpu.nix
    ./appimages.nix
    ./autorun.nix
    ./boot.nix
    ./displaymanager.nix
    ./distrobox.nix
    ./flatpak.nix
    ./helix.nix
    ./hwclock.nix
    ./intel-amd.nix
    ./intel-gpu.nix
    ./intel-nvidia.nix
    ./kernel.nix
    ./libreoffice.nix
    ./logitech.nix
    ./nfs.nix
    ./ntp.nix
    ./nvidia.nix
    ./packages.nix
    ./polkit.nix
    ./python.nix
    ./printer.nix
    ./quickemu.nix
    ./services.nix
    ./steam.nix
    ./sway.nix
    ./syncthing.nix
    #./wfetch.nix
    ./vm.nix
    ./vscode.nix
  ];
}
