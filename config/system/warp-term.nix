{ pkgs, config, lib, ... }:

let inherit (import ../../options.nix) warp-term;

in lib.mkIf (warp-term == true) {
  environment.systemPackages = with pkgs;
    [

      pkgs.warp-terminal

    ];

}

