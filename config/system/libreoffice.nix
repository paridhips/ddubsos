{ pkgs, config, lib, ... }:

let inherit (import ../../options.nix) libreoffice;

in lib.mkIf (libreoffice == true) {
  environment.systemPackages = with pkgs;
    [

      pkgs.libreoffice-fresh

    ];

}

