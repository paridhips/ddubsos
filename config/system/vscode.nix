{ config, pkgs, lib, ... }:

let inherit (import ../../options.nix) vscode;

in lib.mkIf (vscode == true) {
  environment.systemPackages = with pkgs; [
    pkgs.vscode
    pkgs.direnv
    pkgs.taplo
  ];
}

