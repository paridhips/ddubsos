{ pkgs, config, lib, inputs, ... }:

let inherit (import ../../options.nix) steam;

in lib.mkIf (steam == true) {

  programs = { steam.gamescopeSession.enable = true; };

  # Steam Configuration
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };
}
