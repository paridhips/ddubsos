{ pkgs, config, lib, ... }:

let inherit (import ../../options.nix) helix;

in lib.mkIf (helix == true) {
  environment.systemPackages = with pkgs; [

    pkgs.helix
    pkgs.nodePackages_latest.bash-language-server
    pkgs.nodePackages_latest.vscode-json-languageserver
    pkgs.nodePackages.stylelint
    pkgs.nodePackages.vim-language-server
    pkgs.nodePackages.vscode-langservers-extracted
    pkgs.nixfmt
    pkgs.lua-language-server
    pkgs.python311Packages.python-lsp-server
    pkgs.dockerfile-language-server-nodejs
    pkgs.nil
    pkgs.taplo
    pkgs.marksman
    pkgs.clang-tools

  ];

}

