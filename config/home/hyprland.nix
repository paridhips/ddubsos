{ pkgs, config, lib, inputs, ... }:

let
  theme = config.colorScheme.palette;
  hyprplugins = inputs.hyprland-plugins.packages.${pkgs.system};
  inherit (import ../../options.nix)
    browser cpuType gpuType wallpaperDir borderAnim theKBDLayout terminal ModKey
    theSecondKBDLayout hostname theKBDVariant sdl-videodriver;
in with lib; {
  wayland.windowManager.hyprland = {
    enable = true;
    xwayland.enable = true;
    systemd.enable = true;
    plugins = [ hyprplugins.hyprtrails hyprplugins.hyprexpo ];
    extraConfig = let
      #modifier = "LALT";
      modifier = ModKey;
    in concatStrings [''
      monitor = ${
        if hostname == "macbook" then ''
          eDP-1,preferred,auto,1
        '' else if hostname == "artemus" then ''
          HDMI-A-1,1920x1080@75,0x0,1
        '' else if hostname == "bubo" then ''
          eDP-1,1920x1080@60,0x0,1
        '' else if hostname == "asus" then ''
          eDP-1,1920x1080@60,auto,1
        '' else if cpuType == "vm" then ''
          Virtual-1,1920x1080@60,auto,1
        '' else ''
          ,preferred,auto,1
        ''

      }    
              windowrule = float, ^(steam)$
              windowrule = size 1080 900, ^(steam)$
              windowrule = center, ^(steam)$
              windowrule = opacity 0.9 0.9, discord
              windowrule = opacity 0.8 0.8, steam
              general {
                gaps_in = 6
                gaps_out = 8
                border_size = 2
                col.active_border = rgba(${theme.base0C}ff) rgba(${theme.base0D}ff) rgba(${theme.base0B}ff) rgba(${theme.base0E}ff) 45deg
                col.inactive_border = rgba(${theme.base00}cc) rgba(${theme.base01}cc) 45deg
                layout = dwindle
                resize_on_border = true
              }

              input {
                kb_layout = ${theKBDLayout}, ${theSecondKBDLayout}
        	kb_options = grp:alt_shift_toggle
                kb_options=caps:super
                follow_mouse = 1
                touchpad {
                  natural_scroll = false
                }
                sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
                accel_profile = flat
              }
              env = NIXOS_OZONE_WL, 1
              env = NIXPKGS_ALLOW_UNFREE, 1
              env = XDG_CURRENT_DESKTOP, Hyprland
              env = XDG_SESSION_TYPE, wayland
              env = XDG_SESSION_DESKTOP, Hyprland
              env = GDK_BACKEND, wayland
              env = CLUTTER_BACKEND, wayland
              env = SDL_VIDEODRIVER, ${sdl-videodriver}
              env = QT_QPA_PLATFORM, wayland
              env = QT_WAYLAND_DISABLE_WINDOWDECORATION, 1
              env = QT_AUTO_SCREEN_SCALE_FACTOR, 1
              env = MOZ_ENABLE_WAYLAND, 1
              ${
                if cpuType == "vm" then ''
                  env = WLR_NO_HARDWARE_CURSORS,1
                  env = WLR_RENDERER_ALLOW_SOFTWARE,1
                '' else
                  ""
              }
              ${
                if gpuType == "nvidia" then ''
                  env = WLR_NO_HARDWARE_CURSORS,1
                '' else
                  ""
              }
              gestures {
                workspace_swipe = true
                workspace_swipe_fingers = 3
              }
              misc {
                mouse_move_enables_dpms = true
                key_press_enables_dpms = false
                enable_swallow = true;
                #force_default_wallpaper = 0;
                #new_window_takes_over_fullscreen = 2;
                disable_hyprland_logo = true;
                disable_splash_rendering = true;
                animate_manual_resizes = true;
                animate_mouse_windowdragging = true
              }
              animations {
                enabled = yes
                bezier = wind, 0.05, 0.9, 0.1, 1.05
                bezier = winIn, 0.1, 1.1, 0.1, 1.1
                bezier = winOut, 0.3, -0.3, 0, 1
                bezier = liner, 1, 1, 1, 1
                animation = windows, 1, 6, wind, slide
                animation = windowsIn, 1, 6, winIn, slide
                animation = windowsOut, 1, 5, winOut, slide
                animation = windowsMove, 1, 5, wind, slide
                animation = border, 1, 1, liner
                ${
                  if borderAnim == true then ''
                    animation = borderangle, 1, 30, liner, loop
                  '' else
                    ""
                }
                animation = fade, 1, 10, default
                animation = workspaces, 1, 5, wind
              }
              decoration {
                rounding = 0
                drop_shadow = false
                blur {
                    enabled = false
                    size = 5
                    passes = 3
                    new_optimizations = on
                    ignore_opacity = on
                }
              }
              plugin {
                hyprtrails {
                  color = rgba(${theme.base0A}ff)
                }
              }

            plugin {
              hyprexpo {
                columns = 3
                gap_size = 5
                 bg_col = rgb(111111)
                  workspace_method = center current # [center/first] [workspace] e.g. first 1 or center m+1
                 enable_gesture = true # laptop touchpad, 4 fingers
                 gesture_distance = 300 # how far is the "max"
                 gesture_positive = true # positive = swipe down. Negative = swipe up.
              }
            }

            bind = CTRL, space, hyprexpo:expo, toggle # can be: toggle, off/disable or on/enable

              exec-once = $POLKIT_BIN
              exec-once = dbus-update-activation-environment --systemd --all
              exec-once = systemctl --user import-environment QT_QPA_PLATFORMTHEME WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
              exec-once = swww init
              #exec-once = pypr
              exec-once = waybar
              #exec-once = waybar -c ~/.config/waybar-dw1/config -s ~/.config/waybar-dw1/style.css &
              exec-once = swaync
              #exec-once = wallsetter
              exec-once = wpaperd
              exec-once = nm-applet --indicator
              exec-once = swayidle -w timeout 720 'swaylock -f' timeout 1000 'hyprctl dispatch dpms off' resume 'hyprctl dispatch dpms on' before-sleep 'swaylock -f -c 000000'
              dwindle {
                pseudotile = true
                preserve_split = true
              }
              master {
                new_is_master = true
              }
              bind = ${modifier},Return,exec,${terminal}
              bind = ${modifier}SHIFT,Return,exec,rofi-launcher
              bind = ${modifier}SHIFT,D,exec,rofi-launcher
              bind = ${modifier}SHIFT,W,exec,web-search
              bind = ${modifier}SHIFT,N,exec,swaync-client -rs
              ${
                if browser == "google-chrome" then ''
                  bind = ${modifier},W,exec,google-chrome-stable
                '' else ''
                  bind = ${modifier},W,exec,${browser}
                ''
              }
              bind = ${modifier},E,exec,emopicker9000
              bind = ${modifier},S,exec,screenshootin
              bind = ${modifier},D,exec,discord
              bind = ${modifier},J,exec,jellyfinmediaplayer
              bind = ${modifier},B,exec, appimage-run ~/src/thorium/thorium-browser
              bind = ${modifier}SHIFT,W,exec,wezterm
              bind = ${modifier},A,exec,alacritty
              bind = ${modifier}SHIFT,A,exec,kitty
              bind = ${modifier}SHIFT,C,exec,wpaperd
              bind = ${modifier}CTRL,A,exec,st
              bind = ${modifier},O,exec,obs
              #bind = ${modifier},G,exec,google-chrome-stable
              bind = ${modifier},G,exec,flatpak run com.google.Chrome
              bind = ${modifier}CTRL,M,exec,nwg-drawer
              bind = ${modifier}SHIFT,G,exec,gimp
              bind = ${modifier},T,exec,thunar
              bind = ${modifier},M,exec,spotify
              bind = ${modifier},Q,killactive,
              bind = ${modifier},P,pseudo,
              bind = ${modifier}SHIFT,I,togglesplit,
              bind = ${modifier},F,fullscreen,
              bind = ${modifier}SHIFT,F,togglefloating,
              bind = ${modifier}SHIFT,X,exit,
              bind = ${modifier}SHIFT,left,movewindow,l
              bind = ${modifier}SHIFT,right,movewindow,r
              bind = ${modifier}SHIFT,up,movewindow,u
              bind = ${modifier}SHIFT,down,movewindow,d
              bind = ${modifier}SHIFT,h,movewindow,l
              bind = ${modifier}SHIFT,l,movewindow,r
              bind = ${modifier}SHIFT,k,movewindow,u
              bind = ${modifier}SHIFT,j,movewindow,d
              bind = ${modifier},left,movefocus,l
              bind = ${modifier},right,movefocus,r
              bind = ${modifier},up,movefocus,u
              bind = ${modifier},down,movefocus,d
              bind = ${modifier},h,movefocus,l
              bind = ${modifier},l,movefocus,r
              bind = ${modifier},k,movefocus,u
              bind = ${modifier},j,movefocus,d
              bind = ${modifier},1,workspace,1
              bind = ${modifier},2,workspace,2
              bind = ${modifier},3,workspace,3
              bind = ${modifier},4,workspace,4
              bind = ${modifier},5,workspace,5
              bind = ${modifier},6,workspace,6
              bind = ${modifier},7,workspace,7
              bind = ${modifier},8,workspace,8
              bind = ${modifier},9,workspace,9
              bind = ${modifier},0,workspace,10
              bind = ${modifier}SHIFT,SPACE,movetoworkspace,special
              bind = ${modifier},SPACE,togglespecialworkspace
              bind = ${modifier}SHIFT,1,movetoworkspace,1
              bind = ${modifier}SHIFT,2,movetoworkspace,2
              bind = ${modifier}SHIFT,3,movetoworkspace,3
              bind = ${modifier}SHIFT,4,movetoworkspace,4
              bind = ${modifier}SHIFT,5,movetoworkspace,5
              bind = ${modifier}SHIFT,6,movetoworkspace,6
              bind = ${modifier}SHIFT,7,movetoworkspace,7
              bind = ${modifier}SHIFT,8,movetoworkspace,8
              bind = ${modifier}SHIFT,9,movetoworkspace,9
              bind = ${modifier}SHIFT,0,movetoworkspace,10
              bind = ${modifier}CONTROL,right,workspace,e+1
              bind = ${modifier}CONTROL,left,workspace,e-1
              bind = ${modifier},mouse_down,workspace, e+1
              bind = ${modifier},mouse_up,workspace, e-1
              bindm = ${modifier},mouse:272,movewindow
              bindm = ${modifier},mouse:273,resizewindow
              bind = ALT,Tab,cyclenext
              bind = ALT,Tab,bringactivetotop
              bind = ${modifier}CTRL,J, changegroupactive,f
              bind = ${modifier}CTRL,L, changegroupactive,b
              bind = ${modifier},SPACE, togglespecialworkspace
              bind = ${modifier}SHIFT,SPACE, movetoworkspace,special
        	# Switch workspaces relative to the active workspace with mainMod + CTRL + [←→]
              bind = ${modifier} CTRL, right, workspace, r+1
              bind = ${modifier} CTRL, left, workspace, r-1

        	# move to the first empty workspace instantly with mainMod + CTRL + [↓]
              bind = ${modifier} CTRL, down, workspace, empty
        	
        	# Resize windows
              binde = ${modifier} SHIFT, right, resizeactive, 30 0
              binde = ${modifier} SHIFT, left, resizeactive, -30 0
              binde = ${modifier} SHIFT, up, resizeactive, 0 -30
              binde = ${modifier} SHIFT, down, resizeactive, 0 30

              # Pyprland
              #bind = ${modifier} SHIFT, V, exec, pypr toggle volume
              #bind = ${modifier} SHIFT, S, exec, pypr toggle term
              #bind = ${modifier} SHIFT, B, exec, pypr toggle stb
              #bind = ${modifier} SHIFT, T, exec, pypr toggle stb-logs
              #bind = ${modifier} SHIFT, U, exec, pypr toggle logs
              #bind = ${modifier} SHIFT, R, exec, pypr reload


              bind = ,XF86AudioRaiseVolume,exec,wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+
              bind = ,XF86AudioLowerVolume,exec,wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
              binde = ,XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
              bind = ,XF86AudioPlay, exec, playerctl play-pause
              bind = ,XF86AudioPause, exec, playerctl play-pause
              bind = ,XF86AudioNext, exec, playerctl next
              bind = ,XF86AudioPrev, exec, playerctl previous
              bind = ,XF86MonBrightnessDown,exec,brightnessctl set 5%-
              bind = ,XF86MonBrightnessUp,exec,brightnessctl set +5%
    ''];
  };
}
