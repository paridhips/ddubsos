{ pkgs, config, ... }:

{
  # Place Files Inside Home Directory
  home.file.".emoji".source = ./files/emoji;
  home.file.".base16-themes".source = ./files/base16-themes;
  home.file.".face".source = ./files/face.jpg; # For GDM
  home.file.".face.icon".source = ./files/face.jpg; # For SDDM
  home.file.".config/rofi/rofi.jpg".source = ./files/rofi.jpg;
  home.file.".config/starship.toml".source = ./files/starship.toml;
  home.file.".config/swaylock-bg.jpg".source = ./files/media/swaylock-bg.jpg;
  home.file.".config/ascii-neofetch".source = ./files/ascii-neofetch;
  #
  # My configs
  #
  home.file.".config/helix/config.toml".source = ./files/helix.config.toml;
  home.file.".bashrc-personal".source = ./files/.bashrc-personal;
  home.file.".config/wpaperd/wallpaper.toml".source = ./files/wallpaper.toml;
  home.file.".config/lazygit/config.yml".source = ./files/lazygit/config.yml;
  home.file.".config/fastfetch/config.jsonc".source = ./files/fastfetch2.jsonc;

  home.file.".config/sway" = {
    source = ./files/sway;
    recursive = true;
  };

  home.file.".config/tmux" = {
    source = ./files/tmux;
    recursive = true;
  };

  home.file.".config/waybar-dw1" = {
    source = ./files/waybar-dw1;
    recursive = true;
  };

  home.file.".config/mc" = {
    source = ./files/mc;
    recursive = true;
  };

  home.file.".config/btop" = {
    source = ./files/btop;
    recursive = true;
  };

  home.file.".config/ranger" = {
    source = ./files/ranger;
    recursive = true;
  };

  home.file.".config/putty" = {
    source = ./files/putty;
    recursive = true;
  };

  home.file.".local/share/fonts" = {
    source = ./files/fonts;
    recursive = true;
  };
  home.file.".config/wlogout/icons" = {
    source = ./files/wlogout;
    recursive = true;
  };
  home.file.".config/obs-studio" = {
    source = ./files/obs-studio;
    recursive = true;
  };
}
